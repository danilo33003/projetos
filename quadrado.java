package PadroesProjeto;

public class quadrado implements IFigBiDim {
	
	private int lado;
	
	private boolean condEx(int c) {
		return c > 0;
	}	
	
	public quadrado() {
		this.lado = 1;
	}

	public quadrado(int lado) {
		if(!condEx(lado)) {
			throw new RuntimeException("impossivel construir quadrado");
		}
	this.lado = lado;	
	}

	public int getLado() {
		return lado;
	}
	
	public void setLado(int lado) {
		if(condEx(lado)){
			this.lado = lado;
		}
	}
	
	public double perimetro() {
		return (lado*4);
	}
	
	public double area() {
		return (lado*lado);
	}
	
	public String toString() {
		return("(" + this.lado + ")");
	}
}
