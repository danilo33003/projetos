package PadroesProjeto;

public class onibus implements IVeiculoAutomotor{
	
	private int rodas;
	private String Objetivo;
	
	public onibus() {
		rodas = 6;
		Objetivo = "Transporte coletivo para mais de 20 passageiros";
	}
	
	public int getRoda() {
		return rodas;
	}
	
	public String getObj(){
		return Objetivo;
	}
	
	public int qqtrodas() {
		int r = getRoda();
		return r;
	}
	
	public String objetivo() {
		String s = getObj();
		return s;
	}
}
