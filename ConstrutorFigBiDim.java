package PadroesProjeto;

public class ConstrutorFigBiDim implements IConstrucaoFigBiDim{

	@Override
	public IFigBiDim criar(String nomeFigura) {
		
		if(nomeFigura.equals("triangulo")) {
			return new triangulo();
		}
		if(nomeFigura.equals("retangulo")) {
			return new retangulo();
		}
		if(nomeFigura.equals("circulo")) {
			return new circulo();
		}
		if(nomeFigura.equals("quadrado")) {
			return new quadrado();
		}
	return null;
	}
	
}
