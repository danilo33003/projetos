package PadroesProjeto;

public class retangulo implements IFigBiDim{
	
	private int ladoA;
	private int ladoB;
	
	private boolean condEx(int ladoA,int ladoB) {
		return ladoA > 0 && ladoB > 0;
	}
	
	public retangulo() {
		ladoA = 1;
		ladoB = 1;
	}
		
	public retangulo(int ladoA, int ladoB) {
		if(!condEx(ladoA, ladoB)) {
			throw new RuntimeException("impossivel construir quadrado");
		}
		this.ladoA = ladoA;
		this.ladoB = ladoB;
	}
	
	public double perimetro() {
		return (ladoA * 2) + (ladoB * 2);
	}
	
	public double area() {
		return ladoA * ladoB;
	}
}
