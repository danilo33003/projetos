package Classe;

public class DataNG implements IData{

	
	public byte getDia() {
		
		return 0;
	}

	
	public void setDia(byte dia) {
		
		
	}

	
	public byte getMes() {
		
		return 0;
	}

	
	public void setMes(byte mes) {
		
		
	}


	public short getAno() {
		
		return 0;
	}

	
	public void setAno(short ano) {
	
		
	}
	public boolean equals(Object obj) { 
		if(this == obj) 
            return true;
		
		 if(obj == null || obj.getClass()!= this.getClass()) 
			 return false;
		
		 DataNG d = (DataNG) obj;
		 
		
		 return this.getAno() == d.getAno() && 
				this.getMes() == d.getMes() &&
				this.getDia() == d.getDia();
	 
}

	
	public void incrementaDia() {
		
		
	}

	
	public void incrementaMes() {
	
		
	}

	
	public void incrementaAno() {
		
		
	}

	
	public void incrementaNDia(int n) {
		
		
	}

	
	public void incrementaNMes(int n) {
		
		
	}

	
	public void incrementaNAno(int n) {
		
		
	}

	
	public boolean ehUltimoDiadoano() {
		
		return false;
	}

	
	public boolean ehPrimeiroDiadoano() {
		
		return false;
	 
	}
	public boolean equals(Object obj) {
		
		if(this == obj)
			return true;
		 
		if(obj == null || obj.getClass()!= this.getClass())
			return false;
		
		Data d = (DataNG) obj;
		 
		return this.getAno() == d.getAno() && 
				this.getMes() == d.getMes() && 
				this.getDia() == d.getDia();
	}	
	 
	public boolean lt(Object obj) {
		
		Data d = (DataNG) obj;
		 
		if (
			(this.getAno() < d.getAno()) ||
			(this.getAno() == d.getAno() && this.getMes() < d.getMes()) ||
			(this.getAno() == d.getAno() && this.getMes() == d.getMes() && this.getDia() < d.getDia())
			)
			return true;
		else
			return false;
	}
	 
	public boolean le(Object obj) {
		
		return this.equals(obj) || this.lt(obj);
	}
	 
	public boolean ge(Object obj) {
		
		return !(this.lt(obj));
	 
	}
	
	public boolean gt(Object obj) {
		
		return !(this.le(obj));
	}
	
	public int compare(DataNG d1, DataNG d2) {
		if(d1.lt(d2)) return -1;
		if(d1.equals(d2)) return 0;
		return 1;
	}

	
}
