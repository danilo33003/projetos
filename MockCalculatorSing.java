package PadroesProjeto;

public class MockCalculatorSing {

	private static MockCalculatorSing instance;
	
	private MockCalculatorSing() {
	}
	
	public MockCalculatorSing getInstance() {
		
		if(instance == null) {
			instance = new MockCalculatorSing();
		}

		return instance;

	}
	
	
	
	public int soma(int a, int b) {
		
		return a + b;
	
	}
	
	
	public int subtracao(int a, int b) {
	
		return a - b;
	
	}
	
	public int multiplicacao(int a, int b) {
		return a * b;
		
	}
	
	public int divisao(int a, int b) {
		return a/b;
		
	}
	
}
