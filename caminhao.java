package PadroesProjeto;

public class caminhao implements IVeiculoAutomotor{
	
	private int rodas;
	private String Objetivo;
	
	public caminhao() {
		rodas = 6;
		Objetivo = "Transporte de carga superior a 3500Kg";
	}
	
	public int getRoda() {
		return rodas;
	}
	
	public String getObj(){
		return Objetivo;
	}
	
	public int qqtrodas() {
		int r = getRoda();
		return r;
	}
	
	public String objetivo() {
		String s = getObj();
		return s;
	}
}
