package PadroesProjeto;

public class ConstrutorVA implements IConstrucaoVA {
	
	@Override
	public IVeiculoAutomotor criarVA(String nomeVeiculo) {
		
		if(nomeVeiculo.equals("carro")) {
			return new carro();
		}
		if(nomeVeiculo.equals("caminhao")) {
			return new caminhao();
		}
		if(nomeVeiculo.equals("onibus")) {
			return new onibus();
		}
		if(nomeVeiculo.equals("motocicleta")) {
			return new motocicleta();
		}
	return null;
	}
}

