package PadroesProjeto;


public class MockCalculator {

private static MockCalculator instance;

private MockCalculator() {
}

public MockCalculator getInstance() {
	
	if(instance == null) {
		instance = new MockCalculator();
	}

	return instance;

}



public int soma(int a, int b) {
	
	return a + b;

}


public int subtracao(int a, int b) {

	return a - b;

}

public int multiplicacao(int a, int b) {
	return a * b;
	
}

public int divisao(int a, int b) {
	return a/b;
	
}

}
