package Classe;

 class Horario {
	      
			private byte hora;    
			private byte minuto;  
			private byte segundo;
			
			public Horario(byte hora, byte minuto, byte segundo){
				setHora(hora);
				setMinuto(minuto);
				setSegundo(segundo);
		
			}
	 
			public void setHora(byte hora) {
				
				if(hora >= 0 && hora <= 23) {
					this.hora = hora;
				}
			}
			
			public byte getHora() {
				return this.hora;
			}		

			public void setMinuto(byte minuto) {
				if(minuto >= 0 && minuto <= 59) {
					this.minuto = minuto;
				}
			}
			
			public byte getMinuto() {
				return this.minuto;
			}
			
			public void setSegundo(byte segundo) {
				if(segundo >= 0 && segundo <= 59) {
					this.segundo = segundo;
				}
			}
			
			public byte getSegundo() {
				return this.segundo;
			}
 }
	