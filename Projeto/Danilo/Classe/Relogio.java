package Classe;

public class Relogio {

		private Horario hms;
		private ClasseData dma;
		
		public Relogio(Horario hms, ClasseData dma) {
			this.hms = new Horario (hms);
			this.dma = dma;
		}
		
		public void tictac() {
			
			hms.incrementaSegundo();
			

			if(hms.ehPrimeiroHorario()) {
				dma.incrementaDia();
				dma.incrementaMes();
				dma.incrementaAno();
			}
			hms.incrementaNSegundo(2);
			hms.incrementaNMinuto(2);
			hms.incrementaNHora(2);
		
			dma.incrementaDia();
			
		
			dma.incrementaNDia(2);
			dma.incrementaNMes(2);
			dma.incrementaNAno(2);
			
		}

				
		public String toString() {
			return dma + " " + hms;
		}
	} 
