 package Classe;

public class HorarioAtribMet {
       
		private byte hora;    // {0 .. 23} 
		private byte minuto;  // {0 .. 59}
		private byte segundo; // {0 ... 59}
		  
		
		public HorarioAtribMet (byte hora, byte minuto, byte segundo){
			setHora(hora);       // {0 .. 23}
			setMinuto(minuto);   // {0 .. 59}
			setSegundo(segundo); // {0 .. 59}
		}
		
}