package Classe;

public class Novaclasse {
	private int minutostotalemhoras;      // {1440}
	private int segundostotalemsegundos;   // {60}
	private int segundostotalemminutos;  // {86400}
	
	  

	public Novaclasse (){
		setMinutostotalemhoras(minutostotalemhoras);
		setSegundostotalemsegundos(segundostotalemsegundos);
		setSegundostotalemminutos(segundostotalemminutos);
	
	} 
	
	public void setMinutostotalemhoras(int Minutostotalemhoras) {
		
		if(Minutostotalemhoras >= 0 && Minutostotalemhoras <= 1440) { 
			this.minutostotalemhoras = Minutostotalemhoras;
		}
	}
	public int getMinutostotalemhoras() {
		return this.minutostotalemhoras;
	}	
	
    public void setSegundostotalemsegundos(int Segundostotalemsegundos) {
		
		if(Segundostotalemsegundos >= 0 && Segundostotalemsegundos<= 60) { 
			this.segundostotalemsegundos = Segundostotalemsegundos;
		}
    }
		public int getSegundostotalemsegundos() {
			return this.segundostotalemsegundos;
}
		
        public void setSegundostotalemminutos(int Segundostotalemminutos) {
		
		if(Segundostotalemminutos >= 0 && Segundostotalemminutos <= 86400) { 
			this.segundostotalemminutos = Segundostotalemminutos;
		}
	}
	public int getSegundostotalemminutos() {
		return this.segundostotalemminutos;
	}		 
	
	public String tostring() { 
    
		
		return getMinutostotalemhoras() + "*" + getSegundostotalemsegundos() + "=" + getSegundostotalemminutos();
	
	}	

	public void incrementaMinutostotalemhoras() {

	int h = (int)(minutostotalemhoras + 1);
	
	if(h == 1440) {
		
		minutostotalemhoras = 0;
		getSegundostotalemminutos();
	}else {
		
		minutostotalemhoras = h; 
	}
}

	public void incrementaSegundostotalemsegundos() {

		short s = (short)(segundostotalemsegundos + 1);
		
		if(s == 60) {
			
			segundostotalemsegundos = 0;
			incrementaSegundostotalemMinutos();
			
		}else {
			
			segundostotalemsegundos = s; 
		}
}
	
	public void incrementaSegundostotalemMinutos() {
		int m = (int)(segundostotalemminutos + 1);
	
	if(m == 86400) {
		segundostotalemminutos = 0;
		
	}else {
		segundostotalemminutos = m;
	}
	}
		
		
	}
