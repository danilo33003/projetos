package Classe;     

public class Segundosdodia { 

 
	  
	private int segundo;
 
	  
 
	public Segundosdodia () { 
		setSegundo((byte)0);
	} 
	  
	public Segundosdodia (byte hora, byte minuto, byte segundo) { 
		setHora(hora);
		setMin(minuto);
		setSegundo(segundo);
	}  
 
	
	public void setHora(byte hora) {  
		if(hora >=0 && hora <=23) {
			int h = segundo/3600;			
			int m = (segundo - h*3600)/60;
			int s = (segundo - h*3600) - m*60;
			segundo = hora*3600 + m*60 + s;		
		  
		}  
	}	
	public void setMin(byte minuto) {  
		if(minuto >=0 && minuto <=59) { 
			int h = segundo/3600;
			int m = (segundo - h*3600)/60;
			int s = segundo - h*3600 - m*60;
			segundo = h*3600 + s + minuto*60;
		
		}  
	}  
	public void setSegundo(byte seg) { 
		if(seg >=0 && seg<=59) { 
			int h = segundo/3600;
			int m = (segundo - h*3600)/60;
			segundo = h*3600 + seg + m*60;
		} 
	}  
	 
	
	public int getHora() { 
		int var;
		var = (segundo/3600);
		return var;
	}    
	public int getMinuto() { 
		int minuto;
		int hora;
		hora = (segundo/3600);
		minuto = (segundo - hora*3600)/60;
		return minuto; 
	}   
	public int getSeg() {  
		int hora;
		int min;
		int seg;
		hora = (segundo/3600);
		min = (segundo - hora*3600)/60;
		seg = (segundo - hora*3600 - min*60);
		return seg;
	}  
 
	public String toString() { 
		return getHora() + ":" + getMinuto() + ":" + getSeg();
	} 
	
  
	
	public void incrementaSegundo() { 
		if (segundo < 86400) {
			segundo = segundo + 1;
		} 
	} 
	public void incrementaMinuto() { 
		if (segundo/60 < 1440) {
			segundo = segundo + 60;
		} 
	}   
	public void incrementaHora() { 
		if (segundo/3600 < 24) {
			segundo = segundo + 3600;
		} 
	}
}  
  
