package Classe;

public class HorarioAtribMet2 { 
      
	private byte hora;    // {0 .. 23} 
	private byte minuto;  // {0 .. 59}
	private byte segundo; // {0 ... 59}
	  

	public HorarioAtribMet2 (){
		setHora(hora);       // {0 .. 23}
		setMinuto(minuto);   // {0 .. 59}
		setSegundo(segundo); // {0 .. 59}
	} 
	
	public void setHora(byte hora) {
		
		if(hora >= 0 && hora <= 23) { 
			this.hora = hora;
		}
	}
	 
	public byte getHora() {
		return this.hora;
	}		 

	public void setMinuto(byte minuto) {
		if(minuto >= 0 && minuto <= 59) {
			this.minuto = minuto;
		}
	}
	 
	public byte getMinuto() {
		return this.minuto;
	}
	 
	public void setSegundo(byte segundo) {
		if(segundo >= 0 && segundo <= 59) {
			this.segundo = segundo;
		}
	}
	 
	public byte getSegundo() {
		return this.segundo;
	}
	 
	public String toString() { 
    
		
		return getHora() + ":" + getMinuto() + ":" + getSegundo();
	
	}	

	public void incrementaSegundo() {

	byte s = (byte)(segundo + 1);
	
	if(s == 60) {
		segundo = 0;
		incrementaMinuto();
	}else {
		segundo = s;
	}
}
	
    public void incrementaMinuto() {
		byte m = (byte)(minuto + 1);
	
	if(m == 60) {
		minuto = 0;
		incrementaHora();
	}else {
		minuto = m;
	}
	}
	
    public void incrementaHora() {
		byte h = (byte)(hora + 1);
	
	if(h == 24) {
		hora = 0;
		
	}else {
		hora = h;
	}
}
}