package Classe;

public class Data implements IData {

	private byte dia; 	// {1 .. {28, 29, 30 ou 31} }
	private byte mes; 	// {1 .. 12}
	private short ano; 	// {1 .. 9999}

	private boolean ehBissexto(short ano) {
		return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
	}

	private byte getUltimoDia(byte mes, short ano) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31}; 
		
		if(mes == 2 && ehBissexto(ano)) {
			return 29;
		}
		
		return ud[mes];
	}
	
	public Data() {
		setAno((byte)1);
		setMes((byte)1);
		setDia((byte)1);
	}
	
	public Data(byte dia, byte mes, short ano) {
		this();
		setAno(ano);
		setMes(mes);
		setDia(dia);
	}
	public Data(int dia, int mes, int ano) {
		this((byte)dia, (byte)mes, (short)ano);
	}
	public Data(IData data) {
		this(data.getAno(), data.getMes(), data.getDia());
	}

	
	public byte getDia() {
		return dia;
	}

	
	public void setDia(byte dia) {
		
		byte ultimoDia = getUltimoDia(mes, ano);
		
		if(dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
		}
	}

	
	public byte getMes() {
		return mes;
	}


	public void setMes(byte mes) {
		if(mes >= 1 && mes <= 12) {
			this.mes = mes;
		}
	}


	public short getAno() {
		return ano;
	}

	
	public void setAno(short ano) {
		if(ano >= 1 && ano <= 9999) {
			this.ano = ano;			
		}
	}
	

	
	public String toString() {
		return getDia() + "/" + getMes() + "/" + getAno();
	}
		
		public boolean equals(Object obj) { 
			if(this == obj) 
	            return true;
			
			 if(obj == null || obj.getClass()!= this.getClass()) 
				 return false;
			
			 Data d = (Data) obj;
			 
			 
			 return this.getAno() == d.getAno() && 
					this.getMes() == d.getMes() &&
					this.getDia() == d.getDia(); 
		
	}
	
	
	
	public void incrementaDia() {

		byte ultimoDia = getUltimoDia(mes, ano);
		
		if (dia == ultimoDia) {
			if(mes == 13) {
				setAno((byte)(ano + 1));
				setMes((byte)(mes + 1));
				setDia((byte)(dia = 1));
			}
			else{
				setMes((byte)(mes + 1));
				setDia((byte)(dia = 1));
			}
		}
		else {
			setDia((byte)(dia + 1));
		}
	}
	

	    
		public void incrementaMes() {
			byte m = (byte)(mes + 1);
		
		if(m == 13) {
			
			setAno((byte)(ano + 1));
			setMes((byte)(mes = 1));
		}else {
			mes = m;
			
		}
		}
		
	    
		public void incrementaAno() {
			short a = (short)(ano + 1);
		
		if(a == 10000) {
		
			setAno((byte)(ano = 0));
			
		}else {
			ano = a;
		}
		}
	   
	    public void incrementaNDia(byte n) {
			byte contador = 0;
			while (0 < contador) {
			while (contador < n) {
				incrementaDia();
				contador++;
			}
		}
		
		public void incrementaNMes(byte n) {
			byte contador = 0;
			while (0 < contador) {
			while (contador < n) {
				incrementaMes();
				contador++;
			}
		}
		
		public void incrementaNAno(byte n) {
			byte contador = 0;
			while (0 < contador) {
			while (contador < n) {
				incrementaAno();
				contador++;
			}

			}
	    
		public boolean ehUltimoDiadoano() {
			return dia == 31 && mes == 12 && ano == 2021;
		}

		
		public boolean ehPrimeiroDiadoano() {
			return dia == 1 && mes == 1 && ano == 2022;
		}
		 
		public boolean equals(Object obj) {
			
			if(this == obj)
				return true;
			
			if(obj == null || obj.getClass()!= this.getClass())
				return false;
			
			Data d = (Data) obj;
			
			return this.getAno() == d.getAno() && 
					this.getMes() == d.getMes() && 
					this.getDia() == d.getDia();
		}	
		 
		public boolean lt(Object obj) {
			
			Data d = (Data) obj;
			
			if (
				(this.getAno() < d.getAno()) ||
				(this.getAno() == d.getAno() && this.getMes() < d.getMes()) ||
				(this.getAno() == d.getAno() && this.getMes() == d.getMes() && this.getDia() < d.getDia())
				)
				return true;
			else
				return false;
		}
		 
		public boolean le(Object obj) {
			
			return this.equals(obj) || this.lt(obj);
		}
		
		public boolean ge(Object obj) {
			 
			return !(this.lt(obj));
		
		}
		
		public boolean gt(Object obj) {
			
			return !(this.le(obj));
		}
		
		public int compare(Data d1, Data d2) {
			if(d1.lt(d2)) return -1;
			if(d1.equals(d2)) return 0;
			return 1;
		}

		
		
	}


	



 
